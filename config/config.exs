# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :pwd_reset,
  ecto_repos: [PwdReset.Repo]

# Configures the endpoint
config :pwd_reset, PwdResetWeb.Endpoint,
  url: [host: System.get_env("HOSTNAME")],
  secret_key_base: System.get_env("SECRET_KEY_BASE"),
  render_errors: [view: PwdResetWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: PwdReset.PubSub, adapter: Phoenix.PubSub.PG2]

config :pwd_reset, PwdReset.Mailer,
  adapter: Bamboo.MailgunAdapter,
  api_key: System.get_env("MAILGUN_API_KEY"),
  domain: System.get_env("MAILGUN_DOMAIN")

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
