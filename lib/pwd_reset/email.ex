defmodule PwdReset.Email do
  import Bamboo.Email
  use Bamboo.Phoenix, view: PwdReset.EmailView

  defp base_email do
    new_email()
    |> from("Lauri Laik<reset@lauri.in>")
  end

  def password_reset_email(email, url) do
    base_email()
    |> to(email)
    |> subject("Password reset")
    |> put_text_layout({PwdReset.EmailView, "email.text"})
    |> render("reset.text", url: url)
  end
end
