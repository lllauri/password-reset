defmodule PwdReset.Mailer do
  use Bamboo.Mailer, otp_app: :pwd_reset
end
