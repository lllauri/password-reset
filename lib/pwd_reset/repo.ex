defmodule PwdReset.Repo do
  use Ecto.Repo,
    otp_app: :pwd_reset,
    adapter: Ecto.Adapters.Postgres
end
