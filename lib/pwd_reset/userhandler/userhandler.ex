import Ecto.Query

defmodule PwdReset.UserHandler do
  alias PwdReset.User
  alias PwdReset.Repo

  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  def get_user_by_email(email) do
    query = from u in User, where: u.email == ^email
    user = Repo.one(query)
    case user do
      nil -> {:error, "User not found."}
      %User{} -> {:ok, user}
    end
  end

  def get_user_by_reset_token(token) do
    query = from u in User, where: u.token == ^token
    Repo.one(query)
  end

  def update_user(%User{} = user, attrs \\ %{}) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end
end
