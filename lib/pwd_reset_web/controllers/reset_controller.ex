defmodule PwdResetWeb.ResetController do
  use PwdResetWeb, :controller

  alias Ecto.UUID
  alias PwdReset.UserHandler
  alias PwdReset.Email
  alias PwdReset.Mailer

  # Initial password reset page with user email input
  def index(conn, _params) do
    render conn, "index.html", csrf: get_csrf_token()
  end

  # Check if email/user exists, dispatch an email if it does & apply token
  def show(conn, %{"user" => %{"email" => email}}) do
    user = UserHandler.get_user_by_email(email)

    case user do
      {:error, reason} ->
        conn
        |> put_flash(:error, reason)
        |> redirect(to: Routes.reset_path(conn, :index))
      {:ok, user} ->
        reset_token = UUID.generate()
        {:ok, user} = UserHandler.update_user(user, %{token: reset_token})

        Email.password_reset_email(user.email, Routes.reset_url(conn, :index) <> "/" <> reset_token)
        |> Mailer.deliver_now

        conn
        |> put_flash(:info, "Success! Please check your email for further instructions.")
        |> redirect(to: Routes.reset_path(conn, :index))
    end
  end

  # Render a password reset dialogue if reset token exists
  def check(conn, %{"token" => token}) do
    user = UserHandler.get_user_by_reset_token(token)

    if user do
      conn
      |> render("update.html", email: user.email, csrf: get_csrf_token(), reset_token: token)

    else
      conn
      |> put_status(:not_found)
      |> put_view(PwdResetWeb.ErrorView)
      |> render("404.html")
    end
  end

  # Update password if token is valid and passwords match
  def update(conn, %{"token" => token, "user" => %{"email" => email, "password" => password, "password_confirmation" => password_confirmation}}) do

    user = UserHandler.get_user_by_reset_token(token)

    if password == password_confirmation and user do
      values = %{password: password, token: nil}
      user = UserHandler.update_user(user, values)

      case user do
        {:ok, _user} ->
          conn
          |> put_flash(:info, "Password changed successfully!")
          |> redirect(to: Routes.user_path(conn, :index))
        {:error, changeset} ->
          {key, {reason, _constraint}} = List.first(changeset.errors)
          message = Atom.to_string(key) |> String.capitalize()
          message = message <> " " <> reason <> "."
          conn
          |> put_flash(:error, "Password update failed. " <> message)
          |> render("update.html", email: email, csrf: get_csrf_token(), reset_token: token)
      end

    else
      conn
      |> put_flash(:error, "Passwords did not match.")
      |> render("update.html", email: email, csrf: get_csrf_token(), reset_token: token)
    end

  end
end
