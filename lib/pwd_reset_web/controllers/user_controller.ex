defmodule PwdResetWeb.UserController do
  use PwdResetWeb, :controller

  alias PwdReset.UserHandler

  def index(conn, _params) do
    render conn, "index.html"
  end

  def show(conn, _params) do
    render conn, "show.html", csrf: get_csrf_token()
  end

  def login(conn, %{"user" => %{"email" => email, "password" => password}}) do
    user = UserHandler.get_user_by_email(email)
    case user do
      {:error, reason} ->
        conn
        |> put_flash(:error, reason)
        |> redirect(to: Routes.user_path(conn, :show))
      {:ok, user} ->
        if user.password == password do
          conn
          |> put_flash(:info, "Succesfully logged in!")
          |> redirect(to: Routes.user_path(conn, :index))
        else
          conn
          |> put_flash(:error, "Wrong password.")
          |> redirect(to: Routes.user_path(conn, :show))
        end
    end
  end

  def new(conn, _params) do
    render conn, "new.html", csrf: get_csrf_token()
  end

  def create(conn, %{"user" => %{"email" => email, "password" => password, "password_confirmation" => password_confirmation}}) do

    if password == password_confirmation do
      values = %{email: email, password: password}
      result = UserHandler.create_user(values)

      case result do
        {:error, changeset} ->
          {key, {reason, _constraint}} = List.first(changeset.errors)
          message = Atom.to_string(key) |> String.capitalize()
          message = message <> " " <> reason <> "."
          conn
          |> put_flash(:error, "User creation failed. " <> message)
          |> redirect(to: Routes.user_path(conn, :new))
        {:ok, user} ->
          conn
          |> put_flash(:info, "User created using email " <> user.email)
          |> redirect(to: Routes.user_path(conn, :index))
      end
    else
      conn
      |> put_flash(:error, "Passwords did not match.")
      |> redirect(to: Routes.user_path(conn, :new))
    end

  end
end
