defmodule PwdResetWeb.Router do
  use PwdResetWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", PwdResetWeb do
    pipe_through :browser

    get "/", UserController, :index
    get "/login", UserController, :show
    post "/login", UserController, :login
    resources "/user", UserController
    get "/reset", ResetController, :index
    post "/reset", ResetController, :show
    get "/reset/:token", ResetController, :check
    post "/reset/:token", ResetController, :update

  end

  # Other scopes may use custom stacks.
  # scope "/api", PwdResetWeb do
  #   pipe_through :api
  # end
end
